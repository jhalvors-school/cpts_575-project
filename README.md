## CPTS 575 Project Files
This repository contains all files related to the CPTS 575 project on GANs for Cybersecurity data by James Halvorsen and Daniel Olds. Some files related to the GAN are inherited from the SuperGAN project, which we have not developed, but have modified to be usable with this project. Not included in this repository are the dataset files (.csv and .h5), which are too large to be hosted on either Bitbucket or Github, but which will be available through an offsite link.

## Program Dependencies
* Ruby for the cleaning script.
* Bash for the `gen-flows.sh` script (or you can invoke `main.py` manually if you're on a Windows system)
* Python3 for all other scripts.
* Keras, Pandas, h5py, Numpy, and Scikit-Learn for most Python scripts.
* Plotting module I think needs Matplotlib, but I do not use this. 

## File Descriptions
The following is a description of each file and their purpose in this project.

* `benchmark-gan.py` - Trains an LSTM classifier on the dataset produced by the GAN, and produces performance metrics for the new classifier and the previously trained classifier. Depends on the files `flow-classifier.h5`, `holdout-set.h5`, and all `TYPENAME-generator-output.h5` files in the `generated-flows` directory.
* `clean-dataset.rb` - A Ruby program designed to clean up certain flaws in the dataset, such as missing port values. As this was the first script I had introduced to this codebase, it takes command line arguments instead of assuming a filename. Should be run as `ruby clean-dataset.rb UNSW-NB15_1.csv cleaned.csv`.
* `gen-flows.sh` - A shell script to invoke the GAN to generate a datset for each label.
* `input-module.py` - Part of SuperGAN. This has not been modified. Its purpose is to parse the `.txt` files in the `configs` directory.
* `labels.txt` - A generated text file that shows which one hot vectors are associated with which values for categorical data. This is so that I can recreate the dataset if necessary. Note that IP addresses had been encoded as integers (big endian) when first cleaning the data (I did this *before* realizing that one hot vectors might make sense, and never bothered to remove this), so there may be an additional step in transforming, for instance, `2130706433` into `127.0.0.1`.
* `main.py` - Part of SuperGAN. This program contains the main program logic for the GAN. It has been modified to generate output in the form of `.h5` files in the `generated-flows` directory. This program should be invoked by the `gen-flows.sh` script.
* `models.py` - Part of SuperGAN. This is responsible for creating the discriminator and generator for the GAN. It has been modified to not cause errors when `num_channels` is not 3. This makes it more flexible for datasets other than the sensor data it was designed for.
* `output-labels.py` - Generates `labels.txt`. Depends on `cleaned.csv`.
* `plotting_module.py` - Part of SuperGAN. Can be used for generating visualizations but is unused.
* `process-dataset.py` - Performs further transformations on the cleaned dataset to render it usable by the GAN. In particular, it encodes most categorical data as one hot vectors, and converts netflows of the same type into a time series of 3 flows. Network ports are left alone as integers, because there are around 64K unique ports in the dataset, and this would produce extremely high dimensional data. This program takes no command line arguments, and operates on a `cleaned.csv` file to yield a `processed.h5` file.
* `saving-module.py` - Part of SuperGAN. This file is used to save the weights for a generator, however it is unused in this project.
* `train-initial-classifier.py` - Produces an initial LSTM classifier that is both needed to train the GAN, and needed to compare performance statistics. No command line arguments are used, however this program assumes that `processed.h5` has been created, and outputs `flow-classifier.h5`. It also saves its holdout set as `holdout-set.h5`.
* `training-module.py` - Part of SuperGAN. This is used to train the generator and discriminator. It has been modified slightly to allow replacement when selecting batches of real data. This is both needed for classes that do not have many instances, and also to significantly decrease the amount of time the GAN takes to converge.

## How to Perform the GAN Experiment:
The following steps should be used to repeat the experiment taken.

1. Acquire the initial file UNSW-NB15_1.csv from [here](https://www.unsw.adfa.edu.au/unsw-canberra-cyber/cybersecurity/ADFA-NB15-Datasets/UNSW-NB15_1.csv). 
2. Run `ruby clean-dataset UNSW-NB15_1.csv cleaned.csv` and then `python3 process-dataset.py` to prepare the dataset. This yields `processed.h5`
3. Train an initial classifier using `train-initial-classifier.py` to yield `flow-classifier.h5` and `holdout-set.h5`
4. Train a GAN on each type in the dataset by running `gen-flows.sh`. This will yield a number of `.h5` files in the `generated-flows` directory.
5. Train a second classifier on the GAN dataset using `benchmark-gan.py`. This will yield the output performance of each classifier. Note that there will be two types of accuracy figures listed. Binary accuracy, along with precision, recall, and false positive rate, are concerned with labeling as attack data (positive label) or normal data (negative label). Total accuracy concerns all 10 labels present in the dataset (there are 9 types of attacks, so if total accuracy differs from binary accuracy, some attacks have been misclassified as other attacks).

## Program Output:
The following details the output we have obtained from the benchmark script. Future runs of the program may produce different results, as we have not fixed the random seed in any of the programs.
```
Evaluation for GAN Model:
-------------------------
Total Accuracy:  0.9694428571428572
Binary Accuracy:  0.9695285714285714
Precision:  0.5925925925925926
Recall:  0.007483629560336763
FPR:  0.0001620936606642893
-------------------------
Evaluation for Original Model:
------------------------------
Total Accuracy:  0.9694714285714285
Binary Accuracy:  0.9695
Precision:  0.6666666666666666
Recall:  0.002806361085126286
FPR:  4.420736199935162e-05
------------------------------
```

## Want to use our Generated Files?

If you would like to use the .h5 and .csv files generated in the run that produced the above output, they are available on a Google Drive [here](https://drive.google.com/open?id=1StyrykYC6PxJhwRD_SrtyQOG8ujknlR7). 
