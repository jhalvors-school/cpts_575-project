#!/usr/bin/env python3
# Convert a cleaned dataset into a properly formatted H5 file.

import pandas as pd
import numpy as np
import h5py

# Create a one hot vector from a column in a Pandas dataframe
def to_onehot(column):
	vectors = {}
	leading = 0
	# Convert each class into a vector with leading zeroes
	for value in column:
		if value not in vectors:
			vec = []
			for _ in range(0, leading):
				vec.append(0.0)
			vec.append(1.0)
			vectors[value] = vec
			leading += 1
	# Apply trailing zeroes in vectors
	for key in vectors:
		vec = vectors[key]
		siz = len(vec)
		for _ in range(siz, leading):
			vec.append(0.0)
	# Yield mapping of class -> one hot vector
	return vectors

# Read cleaned dataset produced by Ruby script
cleaned = pd.read_csv("cleaned.csv")

# Get one-hot representations of each class:
proto_onehot = to_onehot(cleaned["proto"])
src_ip_onehot = to_onehot(cleaned["srcip"])
dst_ip_onehot = to_onehot(cleaned["dstip"])
service_onehot = to_onehot(cleaned["service"])
attack_cat_onehot = to_onehot(cleaned["attack_cat"])

# Get numeric labels to use with "y" field of H5 file
num = 0
number_labels = {}
for key in attack_cat_onehot:
	number_labels[key] = num
	num += 1

# Convert a row from the dataset into a flat array of features.
def to_feature_array(row):
	features = []

	# Source IP, Port
	for bit in src_ip_onehot[row["srcip"]]:
		features.append(bit)
	features.append(float(row["sport"]))

	# Destination IP, Port
	for bit in dst_ip_onehot[row["dstip"]]:
		features.append(bit)
	features.append(float(row["dsport"]))

	# Protocol, but skip State
	for bit in proto_onehot[row["proto"]]:
		features.append(bit)
	
	# Features up to Service
	for feature in ["dur","sbytes","dbytes","sttl","dttl","sloss","dloss"]:
		features.append(float(row[feature]))
	
	# Service one hot vector
	for bit in service_onehot[row["service"]]:
		features.append(bit)

	# Everything else but the label
	for feature in ["Sload", "Dload", "Spkts", "Dpkts", "swin", "dwin",
		"stcpb", "dtcpb", "smeansz", "dmeansz", "trans_depth",
		"res_bdy_len","Sjit", "Djit", "Stime", "Ltime", "Sintpkt",
		"Dintpkt","tcprtt", "synack", "ackdat", "is_sm_ips_ports",
		"ct_state_ttl", "ct_flw_http_mthd", "is_ftp_login",
		"ct_ftp_cmd", "ct_srv_src", "ct_srv_dst", "ct_dst_ltm",
		"ct_src_ltm", "ct_src_dport_ltm", "ct_dst_sport_ltm",
		"ct_dst_src_ltm"]:
		features.append(float(row[feature]))

	return features

# Create a time series dataset for each class consisting of 3 netflows, all with
# the same class label. 
xs = []
ys = []
y_onehots = []
for key in attack_cat_onehot:
	print("Processing %s" % key)
	series = []
	subset = cleaned.loc[cleaned["attack_cat"] == key]
	siz = len(subset)
	for i in range(0, siz - (siz%3), 3):
		triple = []
		triple.append(to_feature_array(subset.iloc[i]))
		triple.append(to_feature_array(subset.iloc[i+1]))
		triple.append(to_feature_array(subset.iloc[i+2]))
		series.append(triple)
	count = len(series)
	xs += series
	ys += ([number_labels[key]] * count)
	y_onehots += ([attack_cat_onehot[key]] * count)

xs = np.array(xs)
ys = np.array(ys)
y_onehots = np.array(y_onehots)

# Output as H5
hf = h5py.File("processed.h5", "w")
hf.create_dataset("X", data=xs)
hf.create_dataset("y", data=ys)
hf.create_dataset("y_onehot", data=y_onehots)
hf.close()
