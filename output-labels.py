#!/usr/bin/env python3
# Spit out the actual classes associated with one hot labels and integer labels.
# This will make it easier to reconstruct data if necessary.

import pandas as pd

# Create a one hot vector from a column in a Pandas dataframe
def to_onehot(column):
	vectors = {}
	leading = 0
	# Convert each class into a vector with leading zeroes
	for value in column:
		if value not in vectors:
			vec = []
			for _ in range(0, leading):
				vec.append(0.0)
			vec.append(1.0)
			vectors[value] = vec
			leading += 1
	# Apply trailing zeroes in vectors
	for key in vectors:
		vec = vectors[key]
		siz = len(vec)
		for _ in range(siz, leading):
			vec.append(0.0)
	# Yield mapping of class -> one hot vector
	return vectors

# Read cleaned dataset produced by Ruby script
cleaned = pd.read_csv("cleaned.csv")

# Get one-hot representations of each class:
proto_onehot = to_onehot(cleaned["proto"])
src_ip_onehot = to_onehot(cleaned["srcip"])
dst_ip_onehot = to_onehot(cleaned["dstip"])
service_onehot = to_onehot(cleaned["service"])
attack_cat_onehot = to_onehot(cleaned["attack_cat"])

# Get numeric labels to use with "y" field of H5 file
num = 0
number_labels = {}
print("ATTACK CAT:")
for (key, value) in attack_cat_onehot.items():
	print("Label: %s Class: %d Onehot: %s" % (key, num, value))
	number_labels[key] = num
	num += 1

print("PROTO:")
for (key, value) in proto_onehot.items():
	print("Label: %s Onehot: %s" % (key, value))

print("SRC IP:")
for (key, value) in src_ip_onehot.items():
	print("Label: %s Onehot: %s" % (key, value))

print("DST IP:")
for (key, value) in dst_ip_onehot.items():
	print("Label: %s Onehot: %s" % (key, value))

print("SERVICE:")
for (key, value) in service_onehot.items():
	print("Label: %s Onehot: %s" % (key, value))

