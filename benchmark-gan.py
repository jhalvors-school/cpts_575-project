#!/usr/bin/env python3
# Benchmark script for generated data from SuperGAN.

import h5py
import keras
import numpy as np
from keras.models import Model, Sequential, load_model
from keras.layers import Dense, LSTM

labels = {
	"none" :           [1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
	"exploits" :       [0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
	"reconnaissance" : [0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
	"dos" :            [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
	"generic" :        [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0],
	"shellcode" :      [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
	"fuzzers" :        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0],
	"worms" :          [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
	"backdoors" :      [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
	"analysis" :       [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]
}

# Load training dataset from GAN
print("Loading dataset from GAN")
x_train = []
y_train = []
for (label, onehot) in labels.items():
	filename = "generated-flows/%s-generator-output.h5" % label
	database = h5py.File(filename, "r")
	xs = database["X"]
	y_train += [onehot] * xs.shape[0]
	x_train += list(xs)

x_train = np.array(x_train)
y_train = np.array(y_train)

# Load testing dataset from previous holdout set
print("Loading testing dataset")
holdout = h5py.File("holdout-set.h5", "r")
x_test  = holdout["X"]
y_test  = holdout["y_onehot"]

# Load the original model as well so that testing can be made.
print("Loading original classifier")
orig_model = load_model("flow-classifier.h5")

# Train the exact same model we used in train-initial-classifier.py
# Only this time, we train it on the generated dataset.
print("Training model from GAN:")
gan_model = Sequential()
gan_model.add(LSTM(200, input_shape=(x_train.shape[1], x_train.shape[2])))
gan_model.add(Dense(10, activation="softmax"))
gan_model.compile(loss="mae", optimizer="adam")

fitted = gan_model.fit(
	x_train, y_train,
	epochs=10, batch_size=200,
	validation_data=(x_test, y_test),
	verbose=2, shuffle=False
)

# Obtain predictions from both models.
print("Generating predictions for GAN model")
gan_predictions = gan_model.predict_classes(x_test)
print("Generating predictions for Original model")
orig_predictions = orig_model.predict_classes(x_test)

# Evaluate a model according to both binary metrics, where none is the negative
# class and everything else is the positive class, and a multiclass metric,
# where accuracy between attack classes is considered.
def evaluate_model(predictions, actual):
	n = actual.shape[0]
	tp,tn,tc,fp,fn,fc=(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
	for i in range(0, n):
		p = predictions[i]
		a = np.argmax(actual[i])
		# Did we classify correctly?
		if p == a:
			tc += 1.0
		else:
			fc += 1.0
		# Did we at least classify binary correctly?
		p_pos = (p != 0)
		a_pos = (a != 0)
		if p_pos:
			if a_pos:
				tp += 1.0
			else:
				fp += 1.0
		else:
			if a_pos:
				fn += 1.0
			else:
				tn += 1.0
	print("Total Accuracy: ", tc/(tc+fc))
	print("Binary Accuracy: ", (tp+tn)/(tp+tn+fn+fp))
	print("Precision: ", tp/(tp+fp))
	print("Recall: ", tp/(tp+fn))
	print("FPR: ", fp/(fp+tn))

print("Evaluation for GAN Model:")
print("-------------------------")
evaluate_model(gan_predictions, y_test)
print("-------------------------")

print("Evaluation for Original Model:")
print("------------------------------")
evaluate_model(orig_predictions, y_test)
print("------------------------------")