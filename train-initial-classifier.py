#!/usr/bin/env python3
# Train the initial classifier to provide as input to SuperGAN
# Also save the holdout set to use for benchmarking

import h5py
import keras
import numpy as np
from math import sqrt
from keras.models import Sequential
from keras.layers import Dense, LSTM
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error

# Load dataset produced by process_dataset.py
dataset = h5py.File("processed.h5", "r")
x = dataset["X"]
y = dataset["y_onehot"]

# Split dataset into 30% testing, 70% training
x_train, x_test, y_train, y_test = train_test_split(
	np.asarray(x),
	np.asarray(y),
	test_size = 0.3
)

# Train LSTM model on dataset
model = Sequential()
model.add(LSTM(200, input_shape=(x_train.shape[1], x_train.shape[2])))
model.add(Dense(10, activation="softmax"))
model.compile(loss="mae", optimizer="adam")

fitted = model.fit(
	x_train, y_train,
	epochs=10, batch_size=200,
	validation_data=(x_test, y_test),
	verbose=2, shuffle=False
)

# Evaluate a model according to both binary metrics, where none is the negative
# class and everything else is the positive class, and a multiclass metric,
# where accuracy between attack classes is considered.
def evaluate_model(predictions, actual):
	n = actual.shape[0]
	tp,tn,tc,fp,fn,fc=(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
	for i in range(0, n):
		p = predictions[i]
		a = np.argmax(actual[i])
		# Did we classify correctly?
		if p == a:
			tc += 1.0
		else:
			fc += 1.0
		# Did we at least classify binary correctly?
		p_pos = (p != 0)
		a_pos = (a != 0)
		if p_pos:
			if a_pos:
				tp += 1.0
			else:
				fp += 1.0
		else:
			if a_pos:
				fn += 1.0
			else:
				tn += 1.0
	print("Total Accuracy: ", tc/(tc+fc))
	print("Binary Accuracy: ", (tp+tn)/(tp+tn+fn+fp))
	print("Precision: ", tp/(tp+fp))
	print("Recall: ", tp/(tp+fn))
	print("FPR: ", fp/(fp+tn))

pred = model.predict_classes(x_test)

print("Model Performance:")
print("------------------")
evaluate_model(pred, y_test)

# Write to H5 file
model.save("flow-classifier.h5")

# Save holdout set to H5 file as well.
holdout = h5py.File("holdout-set.h5", "w")
holdout.create_dataset("X", data=x_test)
holdout.create_dataset("y_onehot", data=y_test)
holdout.close()
