#!/usr/bin/env ruby
# This is a Ruby script for cleaning the UNSW dataset.
# The input is a CSV file, the output is another CSV file.

# Note when parsing in Python:
# foo = pd.read_csv("cleaned.csv", dtype={"sport": int, "dsport": int})

input  = ARGV[0]
output = ARGV[1]

HEADER = "srcip,sport,dstip,dsport,proto,state,dur,sbytes,dbytes" +
         ",sttl,dttl,sloss,dloss,service,Sload,Dload,Spkts,Dpkts" +
         ",swin,dwin,stcpb,dtcpb,smeansz,dmeansz,trans_depth,res_bdy_len,Sjit" +
         ",Djit,Stime,Ltime,Sintpkt,Dintpkt,tcprtt,synack,ackdat" +
         ",is_sm_ips_ports,ct_state_ttl,ct_flw_http_mthd,is_ftp_login" +
         ",ct_ftp_cmd,ct_srv_src,ct_srv_dst,ct_dst_ltm,ct_src_ltm" + 
         ",ct_src_dport_ltm,ct_dst_sport_ltm,ct_dst_src_ltm,attack_cat,Label"


# Convert an IP address into a raw integer that is easy operate on.
def ip_to_integer ip
  ip.split('.').map(&:to_i).pack("CCCC").unpack("N")[0]
end

# Normalize ports to base 10
def fix_broken_port num
  if num.start_with? '0x' then num.to_i(16).to_s
  elsif num.include? "-" then "0"
  else num
  end
end

# Read from file, skipping the initial byte order mark
csvfile = File.open(input, "r:bom|utf-8")
outfile = File.open(output, "w")

# Add the actual column labels
outfile.puts HEADER

# Iterate over entries. No values are strings that contain commas so we can
# process this with simple splits and joins.
csvfile.each_line do |line|
  raw = line.split ','

  # Convert IP addresses into an integer form
  raw[0] = ip_to_integer raw[0] # srcip
  raw[2] = ip_to_integer raw[2] # dstip

  # Fix sport and dsport columns to not have hexadecimals
  raw[1] = fix_broken_port raw[1]
  raw[3] = fix_broken_port raw[3]

  # Remove empty values from attack category
  if raw[47].empty? then
    raw[47] = "None"
  end

  outfile.puts raw.join(",")
end